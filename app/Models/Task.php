<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    use HasFactory, Notifiable ;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'tasks';
    protected $fillable = [
        'id',
        'title',
        'start',
        'end',
        'color',
        'allDay',
        'className',
        'extendedProps',
        'user_id',
        'created_at'
    ];
    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
